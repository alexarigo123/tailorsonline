"use strict";

angular.module("tailorsOnlineApp")
    .filter("hasCategory", function() {
        return function(items, categoryId) {
            if (categoryId)
            {
                var filtered = [];
                angular.forEach(items, function(el) {
                    if(el.categories && el.categories.indexOf(categoryId) > -1)
                    {
                        filtered.push(el);
                    }
                });

                return filtered;
            }
            else
            {
                return items;
            }
        };
    })
    .controller("LoginController", ["$scope", "userFactory", "auth", "$timeout", function($scope, userFactory, auth, $timeout) {
        $scope.currentUser = auth.getCurrentUserName();
        $scope.email = "";
        $scope.password = "";
        $scope.login = function()
        {
            if(!$scope.loginForm.$invalid && $scope.email && $scope.password)
            {
                userFactory.userLogin().authenticate(
                    { email: $scope.email, password: $scope.password },
                    function(response)
                    {
                        $scope.currentUser = auth.getCurrentUserName();
                        $("#loginModal").modal("hide");
                    },
                    function(response)
                    {
                        $scope.loginErrorMessage = response.data.message;
                    });
            }
        };
        $scope.logout = function()
        {
            auth.deleteToken();
            $scope.currentUser = "";
        };
        $scope.registerSuccess = false;
        $scope.invalidEmail = false;
        $scope.register = function()
        {
            $scope.invalidEmail = false;
            if(!$scope.registerForm.$invalid && $scope.register.uname && $scope.register.sex && $scope.register.email && $scope.register.password)
            {
                userFactory.users().create(
                    { name: $scope.register.uname, sex: $scope.register.sex, email: $scope.register.email, cellphone: $scope.register.cellphone || "", password: $scope.register.password },
                    function(response)
                    {
                        $scope.registerSuccess = true;
                        $timeout(function(){$scope.registerSuccess = false;}, 4000);
                    },
                    function(response)
                    {
                        $scope.invalidEmail = true;
                    });
            }
        };
    }])
    .controller("ContactController", ["$scope", "$timeout", "contactFactory", function($scope, $timeout, contactFactory) {
        $scope.showLoading = false;
        $scope.contactError = false;
        $scope.showSuccessMessage = false;
        $scope.sendMessage = function()
        {
            $scope.showLoading = true;
            $scope.contactError = false;
            if(!$scope.contactForm.$invalid && $scope.name && $scope.email && $scope.message)
            {
                contactFactory.contact().send(
                    { name: $scope.name, email: $scope.email, cellphone: $scope.cellphone || "", message: $scope.message },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.showSuccessMessage = true;
                        $timeout(function(){$scope.showSuccessMessage = false;}, 4000);
                    },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.contactError = true;
                    });
            }
        };
    }])
    .controller("OrderController", ["$scope", "$timeout", "orderFactory", "fabricFactory", "auth", "cache", function($scope, $timeout, orderFactory, fabricFactory, auth, cache) {
        $scope.sizes = [ "XS", "S", "M", "L", "XL", "Other" ];
        $scope.selectedDesign = cache.getObject("selectedDesign");
        $scope.order = {};
        $scope.order.name = auth.getCurrentUserName();
        $scope.order.email = auth.getCurrentUserEmail();
        $scope.order.cellphone = auth.getCurrentUserCellphone();
        $scope.fabric = "";
        $scope.order.size = "Other";
        $scope.fabrics = [];
        $scope.fabricsLoaded = false;
        $scope.fabricsError = "";
        $scope.showSuccessMessage = false;
        $scope.orderError = false;
        fabricFactory.fabrics().query(
            {},
            function(response)
            {
                $scope.fabricsLoaded = true;
                if(response.success)
                {
                    if(response.fabrics.length > 0)
                    {
                        $scope.fabrics = response.fabrics;
                    }
                }
                else
                {
                    $scope.fabricsError = "Couldn't load the fabrics.";
                }
            },
            function(response)
            {
                $scope.fabricsLoaded = true;
                $scope.fabricsError = "Couldn't load the fabrics.";
            });
        $scope.makeOrder = function()
        {
            if(!$scope.orderForm.$invalid && $scope.order.name && $scope.order.email && $scope.fabric && $scope.order.size)
            {
                $scope.orderError = false;
                orderFactory.orders().create(
                    {
                        orderedDesign: $scope.selectedDesign._id,
                        name: $scope.order.name,
                        email: $scope.order.email,
                        fabric: $scope.fabric._id,
                        size: $scope.order.size,
                        cellphone: $scope.order.cellphone || "",
                        observations: $scope.order.observations || ""
                    },
                    function(response)
                    {
                        if(response.success)
                        {
                            $scope.showSuccessMessage = true;
                            $timeout(function(){$scope.showSuccessMessage = false;}, 4000);
                        }
                        else
                        {
                            $scope.orderError = true;
                        }
                    },
                    function(response)
                    {
                        $scope.orderError = true;
                    });
            }
        };
    }])
    .controller("DetailsController", ["$scope", "commentFactory", "baseURL", "auth", "cache", function($scope, commentFactory, baseURL, auth, cache) {
        $scope.baseURL = baseURL;
        $scope.designComments = [];
        $scope.userLoggedIn = auth.userLoggedIn();
        $scope.selectedDesign = cache.getObject("selectedDesign");
        $scope.commentsLoaded = false;
        $scope.errorMessage = "";
        $scope.commentsErrorMessage = "";
        $scope.selectedImage = "";
        $scope.selectImage = function(image)
        {
            $scope.selectedImage = image;
        };
        if($scope.selectedDesign)
        {
            if($scope.selectedDesign.images && $scope.selectedDesign.images.length > 0)
            {
                $scope.selectedImage = $scope.selectedDesign.images[0];
            }
            commentFactory.comments().query(
                { postedAt: $scope.selectedDesign._id },
                function(response)
                {
                    $scope.commentsLoaded = true;
                    if(response.success)
                    {
                        $scope.designComments = response.comments;
                    }
                },
                function(response)
                {
                    $scope.commentsLoaded = true;
                    $scope.commentsErrorMessage = "Couldn't load the comments.";
                });
        }
        else
        {
            $scope.errorMessage = "No design selected.";
        }
    }])
    .controller("CommentController", ["$scope", "$timeout", "commentFactory", "auth", "cache",  function($scope, $timeout, commentFactory, auth, cache) {
        $scope.mycomment = {
            author: auth.getCurrentUserName(),
            rating: 1,
            comment: "",
            date: new Date()
        };
        $scope.changeRating = function(rating)
        {
            $scope.mycomment.rating = rating;
            document.getElementById("ratingNew" + rating).checked = true;
        };
        $scope.showLoading = false;
        $scope.showSuccessMessage = false;
        $scope.submitError = "";
        $scope.submitComment = function()
        {
            if(!$scope.commentForm.$invalid && $scope.mycomment.rating && $scope.mycomment.comment)
            {
                $scope.submitError = "";
                $scope.showLoading = true;
                commentFactory.comments().create(
                    {
                        rating: $scope.mycomment.rating,
                        postedAt: cache.getObject("selectedDesign")._id,
                        comment: $scope.mycomment.comment
                    },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.showSuccessMessage = true;
                        $timeout(function(){$scope.showSuccessMessage = false;}, 4000);
                    },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.submitError = "Error submitting the comment.";
                    });
            }
        };
    }])
    .controller("HomeController", ["$scope", "$state", "designFactory", "categoryFactory", "baseURL", "cache", function($scope, $state, designFactory, categoryFactory, baseURL, cache) {
        $scope.baseURL = baseURL;
        $scope.mainCategories = [];
        $scope.secondaryCategories = [ { name: "None", _id: ""} ];
        $scope.errorMessage = "";
        $scope.categoryFilter = "";
        $scope.loaded = false;
        $scope.changeCategoryFilter = function(categoryId)
        {
            $scope.categoryFilter = categoryId;
        };
        $scope.goToDetails = function(design)
        {
            cache.addObject("selectedDesign", design);
            $state.go("app.details", { id: design._id });
        };
        categoryFactory.categories().query(
            function(response)
            {
                if(response.success)
                {
                    var i;
                    $scope.loaded = true;
                    for(i = 0; i < response.categories.length; i++)
                    {
                        if(response.categories[i].main)
                        {
                            response.categories[i].designs = [];
                            $scope.mainCategories.push(response.categories[i]);
                        }
                        else
                        {
                            $scope.secondaryCategories.push(response.categories[i]);
                        }
                    }
                    for(i = 0; i < $scope.mainCategories.length; i++)
                    {
                        (function(i)
                        {
                            designFactory.designs().get(
                                { category: $scope.mainCategories[i]._id },
                                function(response)
                                {
                                    if(response.success && response.designs)
                                    {
                                        $scope.mainCategories[i].designs = response.designs;
                                    }
                                });
                        })(i);
                    }
                }
                else
                {
                    $scope.errorMessage = "Couldn't load the categories.";
                }
            },
            function(response)
            {
                $scope.errorMessage = "Couldn't load the categories.";
            });
    }]);