'use strict';

angular.module('tailorsOnlineApp', ['ui.router', 'ngResource'])
    .config(function($stateProvider, $urlRouterProvider, $qProvider) {
        $qProvider.errorOnUnhandledRejections(false);
        $stateProvider
            .state(
                'app',
                {
                    url:'/',
                    views:
                    {
                        'header':
                        {
                            templateUrl: 'views/header.html'
                        },
                        'content':
                        {
                            templateUrl: 'views/home.html',
                            controller: 'HomeController'
                        },
                        'footer':
                        {
                            templateUrl: 'views/footer.html'
                        }
                    }
                })
            .state(
                'app.aboutus',
                {
                    url:'aboutus',
                    views:
                    {
                        'content@':
                        {
                            templateUrl: 'views/aboutus.html'
                        }
                    }
                })
            .state(
                'app.contactus',
                {
                    url:'contactus',
                    views:
                    {
                        'content@':
                        {
                            templateUrl: 'views/contactus.html',
                            controller: 'ContactController'
                        }
                    }
                })
            .state(
                'app.order',
                {
                    url: 'order',
                    views:
                    {
                        'content@':
                        {
                            templateUrl: 'views/order.html',
                            controller: 'OrderController'
                        }
                    }
                })
            .state(
                'app.details',
                {
                    url: 'details/:id',
                    views:
                    {
                        'content@':
                        {
                            templateUrl: 'views/details.html',
                            controller: 'DetailsController'
                        }
                    }
                });
    $urlRouterProvider.otherwise('/');
});