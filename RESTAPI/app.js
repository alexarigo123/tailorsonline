var express = require("express");
var app = express();
var bodyParser = require("body-parser");
var morgan = require("morgan");
var mongoose = require("mongoose");
var cfenv = require("cfenv");
var mongodbUri = require("mongodb-uri");
var path = require("path");

// =======================
// configuration =========
// =======================
mongoose.Promise = global.Promise;

// load local VCAP configuration  and service credentials
var vcapLocal;
try
{
	vcapLocal = require("./vcap-local.json");
	console.log("Loaded local VCAP", vcapLocal);
}
catch (e) { }

const appEnvOpts = vcapLocal ? { vcap: vcapLocal} : {}
const appEnv = cfenv.getAppEnv(appEnvOpts);
// Connect to database.
if (appEnv.services["compose-for-mongodb"])
{
	var mongodb_services = appEnv.services["compose-for-mongodb"];
	var credentials = mongodb_services[0].credentials;
	var ca = [new Buffer(credentials.ca_certificate_base64, 'base64')];
	var uri = credentials.uri;
	var mongooseConnectString = mongodbUri.formatMongoose(uri);
	var singlemongooseConnectString = mongooseConnectString.split(",")[0];

	mongoose.connect(
		singlemongooseConnectString,
		{
			server: {
				ssl: true,
				sslValidate: true,
				sslCA: ca,
				poolSize: 1,
				reconnectTries: 1
			}
		});
}

// Require the routes
var users = require("./routes/users");
var categories = require("./routes/categories");
var designs = require("./routes/designs");
var comments = require("./routes/comments");
var fabrics = require("./routes/fabrics");
var orders = require("./routes/orders");
var contact = require("./routes/contact");

//Allow CORS
app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*");
    res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, x-access-token");
    next();
});

// Use morgan to log requests to the console.
app.use(morgan("dev"));

// Use body parser so we can get info from POST and/or URL parameters.
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

// Allow express to serve the webpage from the public folder
app.use("/public", express.static(path.join(__dirname, "public")));

// Allow express to serve images from the images folder
app.use("/images", express.static(path.join(__dirname, "images")));

//Setup the routes
app.use("/users", users);
app.use("/categories", categories);
app.use("/designs", designs);
app.use("/comments", comments);
app.use("/fabrics", fabrics);
app.use("/orders", orders);
app.use("/contact", contact);

// Redirect from the base URL to the index.html
app.use("/", function(req, res, next) {
	res.redirect('/public/index.html');
	return;
 });

//Catch 404 and forward to error handler
app.use(function(req, res, next) {
    var err = new Error("Not Found");
    err.status = 404;
    next(err);
});

//Error handlers
app.use(function(err, req, res, next) {
    //Set locals, only providing error in development
    res.locals.message = err.message;
    res.locals.error = req.app.get("env") === "development" ? err : {};

    // render the error page
    res.status(err.status || 500);
    res.json(res.locals);
});

module.exports = app;