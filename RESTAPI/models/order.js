var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Order = new Schema(
    {
        price: { type: Number, required: true },
        name: { type: String, required: true },
        email: { type: String, required: true },
        cellphone: { type: String },
        orderedDesign: { type: mongoose.Schema.Types.ObjectId, ref: "Design" },
        size: { type: String, required: true, enum: [ "XS", "S", "M", "L", "XL", "Other" ] },
        fabric: { type: mongoose.Schema.Types.ObjectId, ref: "Fabric" },
        observations: { type: String }
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("Order", Order);