var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Category = new Schema(
    {
        name: { type: String, required: true, unique: true },
        main: { type: Boolean, default: false }
    }
);

module.exports = mongoose.model("Category", Category);