var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Image = new Schema(
    {
        mimeType: { type: String },
        data: { type: String }
    }
);

var Design = new Schema(
    {
        name: { type: String, required: true },
        images: [ Image ] ,
        price: { type: Number, required: true },
        categories: [ { type: mongoose.Schema.Types.ObjectId, ref: "Category" } ],
        description : { type: String },
        visible: { type: Boolean, required: true }
    }
);

module.exports = mongoose.model("Design", Design);