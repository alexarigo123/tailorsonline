var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var User = new Schema(
    {
        name: { type: String, required: true },
        email: { type: String, required: true, unique: true },
        cellphone: { type: String },
        password: { type: String, required: true },
        sex: { type: String, required: true , enum: [ "Male", "Female" ] },
        admin: { type: Boolean, default: false}
    }
);

module.exports = mongoose.model("User", User);