var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Comment = new Schema(
    {
        rating: { type: Number, required: true, min: 1, max: 5 },
        comment: { type: String, required: true },
        author: { type: String, required: true },
        postedBy: { type: mongoose.Schema.Types.ObjectId, ref: "User" },
        postedAt: { type: mongoose.Schema.Types.ObjectId, ref: "Design" }
    },
    {
        timestamps: true
    }
);

module.exports = mongoose.model("Comment", Comment);