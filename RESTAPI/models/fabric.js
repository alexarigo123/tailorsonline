var mongoose = require("mongoose");
var Schema = mongoose.Schema;

var Fabric = new Schema(
    {
        name: { type: String, required: true, unique: true},
        description: { type: String, required: true }
    }
);

module.exports = mongoose.model("Fabric", Fabric);