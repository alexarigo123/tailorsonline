var express = require("express");
var categoryRouter = express.Router();
var bodyParser = require("body-parser");
categoryRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
categoryRouter.use(fileUpload());
var Category = require("../models/category");
var Security = require("../security");

categoryRouter.route("/")
// Get all the categories.
.get(function(req, res) {
    Category.find({}, function(err, categories) {
        if(err)
        {
            return res.status(500).json({ success: false });
        }
        
        return res.json({ success: true, categories: categories });
    });
})
// Try to create a new user.
.post(Security.verifyOrdinaryUser, Security.verifyAdmin, function(req, res) {
    if (req.body.name)
    {
        var newCategory = new Category({
            name: req.body.name
        });

        // Save the new category.
        newCategory.save(function(err) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            return res.status(200).json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = categoryRouter;