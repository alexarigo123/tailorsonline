var express = require("express");
var fabricRouter = express.Router();
var bodyParser = require("body-parser");
fabricRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
fabricRouter.use(fileUpload());
var Fabric = require("../models/fabric");
var Security = require("../security");

fabricRouter.route("/")
// Get all the fabrics.
.get(function(req, res) {
    Fabric.find({}, function(err, fabrics) {
        if(err)
        {
            return res.status(500).json({ success: false });
        }
        
        return res.json({ success: true, fabrics: fabrics });
    });
})
// Try to create a new user.
.post(Security.verifyOrdinaryUser, Security.verifyAdmin, function(req, res) {
    if (req.body.name && req.body.description)
    {
        var newFabric = new Fabric({
            name: req.body.name,
            description: req.body.description
        });

        // Save the new fabric.
        newFabric.save(function(err) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            return res.status(200).json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = fabricRouter;