var express = require("express");
var userRouter = express.Router();
var bodyParser = require("body-parser");
userRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
userRouter.use(fileUpload());
var User = require("../models/user");
var Security = require("../security");

userRouter.route("/")
// Get all the users.
.get(Security.verifyOrdinaryUser, Security.verifyAdmin, function(req, res) {
    User.find({}, function(err, users) {
        if(err)
        {
            return res.status(500).json({ success: false });
        }

        return res.json({ success: true, users: users });
    });
})
// Try to create a new user.
.post(function(req, res) {
    if (req.body.name && req.body.email && req.body.password && req.body.sex)
    {
        var newUser = new User({
            name: req.body.name,
            email: req.body.email,
            cellphone: req.body.cellphone || "",
            password: Security.bcryptPassword(req.body.password),
            sex: req.body.sex,
            admin: req.body.email.indexOf("@tailorsonline.com") !== -1
        });

        // Save the new user.
        newUser.save(function(err) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            return res.status(200).json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
})
// Try to update the data of a user with the given id in the JWT.
.put(Security.verifyOrdinaryUser, function(req, res) {
    if (req.decoded._doc && req.decoded._doc._id && req.body.name && req.body.password && req.body.sex && req.body.cellphone)
    {
        User.findByIdAndUpdate(req.decoded._doc._id, { $set: req.body }, function(err, user) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            return res.status(200).json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

userRouter.route("/login")
// Try to login with the given email and password and returns a JSON Web Token with the user id if successful.
.post(function(req, res) {
    if (req.body.email && req.body.password)
    {
        User.findOne({ email: req.body.email}, function(err, user) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            if(user)
            {
                // Verify if password matches with the one on the database.
                if (Security.verifyPassword(req.body.password, user.password))
                {
                    // Create the JWT.
                    var token = Security.getToken(user);

                    return res.status(200).json({ success: true, token: token });
                }
                else
                {
                    return res.status(401).json({ success: false, message: "Login failed. Wrong password." });
                }
            }
            else
            {
                return res.status(404).json({ success: false, message: "Login failed. User not found." });
            }
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = userRouter;