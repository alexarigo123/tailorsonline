var express = require("express");
var contactRouter = express.Router();
var bodyParser = require("body-parser");
contactRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
contactRouter.use(fileUpload());
var nodemailer = require("nodemailer");

contactRouter.route("/")
// Try to send an email to the administrator.
.post(function(req, res) {
    if (req.body.name && req.body.email && req.body.message)
    {
        var body = "<b>Name: </b>" + req.body.name + "<br/>";
        body += "<b>Email: </b>" + req.body.email + "<br/>";
        body += "<b>Cellphone: </b>" + (req.body.cellphone || "Not provided") + "<br/>";
        body += "<b>Message:</b><br/>" + req.body.message;
        var mailOptions = {
            from: "contactus.tailorsonline@gmail.com",
            to: "contactus.tailorsonline@gmail.com",
            subject: "Contact Us",
            html: body
        };
        var transporter = nodemailer.createTransport({
            service: "Gmail",
            auth: {
                user: "contactus.tailorsonline@gmail.com",
                pass: "contact_us_tailors_online"
            },
            tls: {
                rejectUnauthorized: false
            }
        });

        transporter.sendMail(mailOptions, function(error, info){
            if(error)
            {
                return res.status(500).json({ success: false });
            }

            return res.json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = contactRouter;