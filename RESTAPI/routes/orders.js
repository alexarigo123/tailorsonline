var express = require("express");
var orderRouter = express.Router();
var bodyParser = require("body-parser");
orderRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
orderRouter.use(fileUpload());
var Order = require("../models/order");
var Design = require("../models/design");
var Fabric = require("../models/fabric");
var Security = require("../security");
var nodemailer = require("nodemailer");

orderRouter.route("/")
// Try to create a new order and sends an email to the admin for him/her to communicate directly with the client.
.post(function(req, res) {
    if (req.body.orderedDesign && req.body.size && req.body.fabric && req.body.name && req.body.email)
    {
        Design.findOne({ _id: req.body.orderedDesign }, function(err, design) {
            if(err)
            {
                return res.status(500).json({ success: false });
            }

            var newOrder = new Order({
                price: design.price,
                orderedDesign: design._id,
                size: req.body.size,
                fabric: req.body.fabric,
                name: req.body.name,
                email: req.body.email,
                cellphone: req.body.cellphone || "",
                observations: req.body.observations || ""
            });

            // Save the new order
            newOrder.save(function(err) {
                if (err)
                {
                    return res.status(500).json({ success: false });
                }

                Fabric.findOne({ _id: req.body.fabric }, function(err, fabric) {
                    var fabricName = "";
                    if (!err)
                    {
                        fabricName = fabric.name;
                    }
                    var body = "<b>Name: </b>" + req.body.name + "<br/>";
                    body += "<b>Email: </b>" + req.body.email + "<br/>";
                    body += "<b>Cellphone: </b>" + (req.body.cellphone || "Not provided") + "<br/>";
                    body += "<b>Size: </b>" + req.body.size + "<br/>";
                    body += "<b>Fabric: </b>" + fabricName + "<br/>";
                    body += "<b>Design Name: </b>" + design.name + "<br/>";
                    body += "<b>Design Code: </b>" + design._id + "<br/>";
                    body += "<b>Observations:</b><br/>" + (req.body.observations || "Not provided");
                    var mailOptions = {
                        from: "contactus.tailorsonline@gmail.com",
                        to: "contactus.tailorsonline@gmail.com",
                        subject: "New Order",
                        html: body
                    };
                    var transporter = nodemailer.createTransport({
                        service: "Gmail",
                        auth: {
                            user: "contactus.tailorsonline@gmail.com",
                            pass: "contact_us_tailors_online"
                        },
                        tls: {
                            rejectUnauthorized: false
                        }
                    });

                    transporter.sendMail(mailOptions, function(error, info) {
                        return res.status(200).json({ success: true });
                    });
                });
            });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = orderRouter;