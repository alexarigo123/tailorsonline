var express = require("express");
var commentRouter = express.Router();
var bodyParser = require("body-parser");
commentRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
commentRouter.use(fileUpload());
var Comment = require("../models/comment");
var Security = require("../security");
var mongoose = require("mongoose");

commentRouter.route("/")
// Get all the comments.
.get(function(req, res) {
    if (req.query && req.query.postedAt)
    {
        Comment.find({ postedAt: mongoose.Types.ObjectId(req.query.postedAt) }, function(err, comments) {
            if(err)
            {
                return res.status(500).json({ success: false });
            }

            return res.json({ success: true, comments: comments });
        });
    }
    else
    {
        res.status(400).json({ success: false });
    }
})
// Try to create a new comment.
.post(Security.verifyOrdinaryUser, function(req, res) {
    console.log(req.body);
    if (req.body.rating && req.body.comment && req.body.postedAt)
    {
        var newComment = new Comment({
            rating: req.body.rating,
            comment: req.body.comment,
            author: req.decoded._doc.name,
            postedAt: req.body.postedAt,
            postedBy: req.decoded._doc._id
        });

        // Save the new comment.
        newComment.save(function(err) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            return res.status(200).json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = commentRouter;