var express = require("express");
var designRouter = express.Router();
var bodyParser = require("body-parser");
designRouter.use(bodyParser.json());
var fileUpload = require("express-fileupload");
designRouter.use(fileUpload());
var Design = require("../models/design");
var Security = require("../security");
var mongoose = require("mongoose");

designRouter.route("/")
// Get the designs for the given category within the category QueryString parameter, the visible value must be true to return the design.
.get(function(req, res) {
    if (req.query && req.query.category)
    {
        Design.find({ visible: true, categories: mongoose.Types.ObjectId(req.query.category) }, function(err, designs) {
            if(err)
            {
                return res.status(500).json({ success: false });
            }

            return res.json({ success: true, designs: designs });
        });
    }
    else
    {
        res.status(400).json({ success: false });
    }
})
// Try to create a new design.
.post(Security.verifyOrdinaryUser, Security.verifyAdmin, function(req, res) {
    if (req.body.name && req.body.price && req.body.categories && req.body.visible && req.body.description && req.files && req.files.images)
    {
        var imagesAux = Array.isArray(req.files.images) ? req.files.images : [ req.files.images ];
        var images = [];
        // Get the base64string from the uploaded images.
        for(var i = 0; i < imagesAux.length; i++)
        {
            console.log(imagesAux[i]);
            images.push({ mimeType: imagesAux[i].mimetype, data: new Buffer(imagesAux[i].data).toString("base64") });
        }

        var newDesign = new Design({
            name: req.body.name,
            price: req.body.price,
            categories: Array.isArray(req.body.categories) ? req.body.categories : [ req.body.categories ],
            visible: req.body.visible,
            description: req.body.description,
            images: images
        });

        // Save the new design.
        newDesign.save(function(err, design) {
            if (err)
            {
                return res.status(500).json({ success: false });
            }

            return res.status(200).json({ success: true });
        });
    }
    else
    {
        return res.status(400).json({ success: false });
    }
});

module.exports = designRouter;