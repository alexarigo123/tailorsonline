var jwt = require("jsonwebtoken"); // used to create, sign, and verify tokens
var config = require("./config"); // get our config file
var User = require("./models/user");
var bcrypt = require("bcrypt-nodejs");

exports.bcryptPassword = function(password) {
    return bcrypt.hashSync(password);
};

exports.verifyPassword = function(password, bcryptedPassword) {
    return bcrypt.compareSync(password, bcryptedPassword);
};

exports.getToken = function(user) {
    if (user)
    {
        return jwt.sign(user, config.secret, { expiresIn: 60 * 60 }); // expiresIn is measured in seconds
    }
    else
    {
        return "";
    }
};

exports.verifyOrdinaryUser = function(req, res, next) {
    // check header or url parameters or post parameters for token
    var token = req.body.token || req.query.token || req.headers["x-access-token"];

    // decode token
    if (token)
    {
        // verifies secret and checks exp
        jwt.verify(token, config.secret, function(err, decoded) {
            if (err)
            {
                var err = new Error("Failed to authenticate token.");
                err.status = 401;
                
                return next(err);
            }
            else
            {
                req.decoded = decoded;
                next();
            }
        });
    }
    else
    {
        // if there is no token
        // return an error
        var err = new Error("No token provided.");
        err.status = 403;
        
        return next(err);
    }
};

exports.verifyAdmin = function(req, res, next) {
    if(req.decoded)
    {
        if(req.decoded._doc.admin)
        {
            next();
        }
        else
        {
            var err = new Error("You are not authorized to perform this operation.");
            err.status = 403;

            return next(err);
        }
    }
    else
    {
        //if there is no token 
        //return an error
        var err = new Error("No token provided.");
        err.status = 403;
        
        return next(err);
    }
};