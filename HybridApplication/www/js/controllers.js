"use strict";

angular.module("tailorsOnlineApp")
    .filter("hasCategory", function() {
        return function(items, categoryId) {
            if (categoryId)
            {
                var filtered = [];
                angular.forEach(items, function(el) {
                    if(el.categories && el.categories.indexOf(categoryId) > -1)
                    {
                        filtered.push(el);
                    }
                });

                return filtered;
            }
            else
            {
                return items;
            }
        };
    })
    .filter("hasMainCategory", function() {
        return function(items, categoryId) {
            if (categoryId)
            {
                var filtered = [];
                angular.forEach(items, function(el) {
                    if(el._id === categoryId)
                    {
                        filtered.push(el);
                    }
                });

                return filtered;
            }
            else
            {
                return items;
            }
        };
    })
    .controller("LoginController", ["$scope", "$ionicModal", "userFactory", "auth", "$timeout", function($scope, $ionicModal, userFactory, auth, $timeout) {
        $scope.currentUser = auth.getCurrentUserName();
        $scope.loginData = {};
        $scope.login = function()
        {
            if($scope.loginData.email && $scope.loginData.password)
            {
                userFactory.userLogin().authenticate(
                    { email: $scope.loginData.email, password: $scope.loginData.password },
                    function(response)
                    {
                        if(response.success)
                        {
                            $scope.currentUser = auth.getCurrentUserName();
                            $scope.loginData = {};
                            $scope.closeLogin();
                        }
                        else
                        {
                            $scope.loginData.loginErrorMessage = response.message;
                            $timeout(function(){$scope.loginData.loginErrorMessage = "";}, 4000);
                        }
                    },
                    function(response)
                    {
                        $scope.loginData.loginErrorMessage = response.data.message || response.message;
                        $timeout(function(){$scope.loginData.loginErrorMessage = "";}, 4000);
                    });
            }
        };
        // Create the login modal that we will use later.
        $ionicModal.fromTemplateUrl('templates/login.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.modal = modal;
        });
        // Triggered in the login modal to close it.
        $scope.closeLogin = function() {
            $scope.modal.hide();
        };
        // Open the login modal.
        $scope.openLogin = function() {
            $scope.modal.show();
        };
        $scope.logout = function()
        {
            auth.deleteToken();
            $scope.currentUser = "";
        };
        $scope.registerData = {};
        $scope.register = function()
        {
            $scope.registerData.invalidEmail = false;
            if($scope.registerData.name && $scope.registerData.sex && $scope.registerData.email && $scope.registerData.password)
            {
                userFactory.users().create(
                    {
                        name: $scope.registerData.name,
                        sex: $scope.registerData.sex,
                        email: $scope.registerData.email,
                        cellphone: $scope.registerData.cellphone || "",
                        password: $scope.registerData.password },
                    function(response)
                    {
                        if(response.success)
                        {
                            $scope.registerData = {};
                            $scope.registerData.registerSuccess = true;
                            $timeout(function(){$scope.registerData.registerSuccess = false;}, 4000);
                            $scope.closeRegister();
                        }
                        else
                        {
                            $scope.registerData.invalidEmail = true;
                        }
                    },
                    function(response)
                    {
                        $scope.registerData.invalidEmail = true;
                    });
            }
        };
        // Create the register modal.
        $ionicModal.fromTemplateUrl('templates/register.html', {
            scope: $scope
        }).then(function(modal) {
            $scope.registerModal = modal;
        });    
        // Triggered in the register modal to close it.
        $scope.closeRegister = function() {
            $scope.registerModal.hide();
        };
    
        // Open the register modal.
        $scope.openRegister = function() {
            $scope.registerModal.show();
        };
    }])
    .controller("ContactController", ["$scope", "$timeout", "contactFactory", function($scope, $timeout, contactFactory) {
        $scope.showLoading = false;
        $scope.contactError = false;
        $scope.showSuccessMessage = false;
        $scope.contactData = {};
        $scope.sendMessage = function()
        {
            $scope.showLoading = true;
            $scope.contactError = false;
            if($scope.contactData.name && $scope.contactData.email && $scope.contactData.message)
            {
                contactFactory.contact().send(
                    { name: $scope.contactData.name, email: $scope.contactData.email, cellphone: $scope.contactData.cellphone || "", message: $scope.contactData.message },
                    function(response)
                    {
                        if(response.success)
                        {
                            $scope.contactData = {};
                            $scope.showLoading = false;
                            $scope.showSuccessMessage = true;
                            $timeout(function(){$scope.showSuccessMessage = false;}, 4000);
                        }
                        else
                        {
                            $scope.showLoading = false;
                            $scope.contactError = true;
                        }
                    },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.contactError = true;
                    });
            }
        };
    }])
    .controller("OrderController", ["$scope", "$timeout", "orderFactory", "fabricFactory", "auth", "cache", function($scope, $timeout, orderFactory, fabricFactory, auth, cache) {
        $scope.sizes = [ "XS", "S", "M", "L", "XL", "Other" ];
        $scope.selectedDesign = cache.getObject("selectedDesign");
        $scope.order = {};
        $scope.order.name = auth.getCurrentUserName();
        $scope.order.email = auth.getCurrentUserEmail();
        $scope.order.cellphone = auth.getCurrentUserCellphone();
        $scope.order.fabric = "";
        $scope.order.size = "Other";
        $scope.fabrics = [];
        $scope.fabricsLoaded = false;
        $scope.fabricsError = "";
        $scope.showSuccessMessage = false;
        $scope.orderError = false;
        fabricFactory.fabrics().query(
            {},
            function(response)
            {
                $scope.fabricsLoaded = true;
                if(response.success)
                {
                    if(response.fabrics.length > 0)
                    {
                        $scope.fabrics = response.fabrics;
                    }
                }
                else
                {
                    $scope.fabricsError = "Couldn't load the fabrics.";
                }
            },
            function(response)
            {
                $scope.fabricsLoaded = true;
                $scope.fabricsError = "Couldn't load the fabrics.";
            });
        $scope.makeOrder = function()
        {
            if($scope.order.name && $scope.order.email && $scope.order.fabric && $scope.order.size)
            {
                $scope.orderError = false;
                orderFactory.orders().create(
                    {
                        orderedDesign: $scope.selectedDesign._id,
                        name: $scope.order.name,
                        email: $scope.order.email,
                        fabric: $scope.order.fabric._id,
                        size: $scope.order.size,
                        cellphone: $scope.order.cellphone || "",
                        observations: $scope.order.observations || ""
                    },
                    function(response)
                    {
                        if(response.success)
                        {
                            $scope.showSuccessMessage = true;
                            $timeout(function(){$scope.showSuccessMessage = false;}, 4000);
                        }
                        else
                        {
                            $scope.orderError = true;
                        }
                    },
                    function(response)
                    {
                        $scope.orderError = true;
                    });
            }
        };
    }])
    .controller("DetailsController", ["$scope", "commentFactory", "baseURL", "auth", "cache", function($scope, commentFactory, baseURL, auth, cache) {
        $scope.baseURL = baseURL;
        $scope.designComments = [];
        $scope.userLoggedIn = auth.userLoggedIn();
        $scope.selectedDesign = cache.getObject("selectedDesign");
        $scope.commentsLoaded = false;
        $scope.errorMessage = "";
        $scope.commentsErrorMessage = "";
        $scope.selectedImage = "";
        $scope.showOrder = false;
        $scope.sortedBy = "";
        $scope.selectImage = function(image)
        {
            $scope.selectedImage = image;
        };
        $scope.toggleOrder = function(sortedBy)
        {
            $scope.showOrder = !$scope.showOrder;
            if(sortedBy)
            {
                $scope.sortedBy = sortedBy === "None" ? "" : sortedBy;
            }
        };
        if($scope.selectedDesign)
        {
            if($scope.selectedDesign.images && $scope.selectedDesign.images.length > 0)
            {
                $scope.selectedImage = $scope.selectedDesign.images[0];
            }
            commentFactory.comments().query(
                { postedAt: $scope.selectedDesign._id },
                function(response)
                {
                    $scope.commentsLoaded = true;
                    if(response.success)
                    {
                        $scope.designComments = response.comments;
                    }
                },
                function(response)
                {
                    $scope.commentsLoaded = true;
                    $scope.commentsErrorMessage = "Couldn't load the comments.";
                });
        }
        else
        {
            $scope.errorMessage = "No design selected.";
        }
    }])
    .controller("CommentController", ["$scope", "$timeout", "commentFactory", "auth", "cache",  function($scope, $timeout, commentFactory, auth, cache) {
        $scope.mycomment = {
            author: auth.getCurrentUserName(),
            rating: 1,
            comment: "",
            date: new Date()
        };
        $scope.changeRating = function(rating)
        {
            $scope.mycomment.rating = rating;
            try
            {
                document.getElementById("ratingNew" + rating).checked = true;
            }
            catch(err){}
        };
        $scope.showLoading = false;
        $scope.showSuccessMessage = false;
        $scope.submitError = "";
        $scope.submitComment = function()
        {
            if(!$scope.commentForm.$invalid && $scope.mycomment.rating && $scope.mycomment.comment)
            {
                $scope.submitError = "";
                $scope.showLoading = true;
                commentFactory.comments().create(
                    {
                        rating: $scope.mycomment.rating,
                        postedAt: cache.getObject("selectedDesign")._id,
                        comment: $scope.mycomment.comment
                    },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.showSuccessMessage = true;
                        $timeout(function(){$scope.showSuccessMessage = false;}, 4000);
                    },
                    function(response)
                    {
                        $scope.showLoading = false;
                        $scope.submitError = "Error submitting the comment.";
                    });
            }
        };
    }])
    .controller("HomeController", ["$scope", "$state", "designFactory", "categoryFactory", "baseURL", "cache", function($scope, $state, designFactory, categoryFactory, baseURL, cache) {
        $scope.baseURL = baseURL;
        $scope.mainCategories = [];
        $scope.secondaryCategories = [ { name: "None", _id: ""} ];
        $scope.errorMessage = "";
        $scope.categoryFilter = "";
        $scope.mainCategoryFilter = "";
        $scope.selectedCategory = "";
        $scope.loaded = false;
        $scope.showFilters = false;
        $scope.toggleFilters = function()
        {
            $scope.showFilters = !$scope.showFilters;
        };
        $scope.changeCategoryFilter = function(categoryName, categoryId)
        {
            $scope.selectedCategory = categoryName;
            $scope.categoryFilter = categoryId;
            $scope.toggleFilters();
        };
        $scope.showMainCategory = function(categoryId)
        {
            $scope.mainCategoryFilter = categoryId;
        };
        $scope.goToDetails = function(design)
        {
            cache.addObject("selectedDesign", design);
            $state.go("app.details", { id: design._id });
        };
        categoryFactory.categories().query(
            function(response)
            {
                if(response.success)
                {
                    var i;
                    $scope.loaded = true;
                    for(i = 0; i < response.categories.length; i++)
                    {
                        if(response.categories[i].main)
                        {
                            if($scope.mainCategoryFilter === "")
                            {
                                $scope.mainCategoryFilter = response.categories[i]._id;
                            }
                            response.categories[i].designs = [];
                            $scope.mainCategories.push(response.categories[i]);
                        }
                        else
                        {
                            $scope.secondaryCategories.push(response.categories[i]);
                        }
                    }
                    for(i = 0; i < $scope.mainCategories.length; i++)
                    {
                        (function(i)
                        {
                            designFactory.designs().get(
                                { category: $scope.mainCategories[i]._id },
                                function(response)
                                {
                                    if(response.success && response.designs)
                                    {
                                        $scope.mainCategories[i].designs = response.designs;
                                    }
                                });
                        })(i);
                    }
                }
                else
                {
                    $scope.errorMessage = "Couldn't load the categories.";
                }
            },
            function(response)
            {
                $scope.errorMessage = "Couldn't load the categories.";
            });
    }]);