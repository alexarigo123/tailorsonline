angular.module('tailorsOnlineApp', ['ionic', 'ngResource'])
.run(function($ionicPlatform, $rootScope, $ionicLoading, $timeout) {
    $ionicPlatform.ready(function() {
        // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
        // for form inputs)
        if (window.cordova && window.cordova.plugins.Keyboard)
        {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar)
        {
        // org.apache.cordova.statusbar required
            StatusBar.styleDefault();
        }
    });
    $rootScope.$on('loading:show', function() {
        $ionicLoading.show({
            template: '<ion-spinner></ion-spinner> Loading ...'
        });
    });

    $rootScope.$on('loading:hide', function() {
        $ionicLoading.hide();
    });

    $rootScope.$on('$stateChangeStart', function() {
        console.log('Loading ...');
        $rootScope.$broadcast('loading.show');
    });

    $rootScope.$on('$stateChangeSuccess', function() {
        console.log('done');
        $rootScope.$broadcast('loading.hide');
    });

    $timeout(function() {
        $state.go('app.home');
    }, 5000);
})
.config(function($stateProvider, $urlRouterProvider) {
    $stateProvider
    .state('app', {
        url: "/app",
        abstract: true,
        templateUrl: "templates/layout.html"
    })
    .state('app.home', {
        url: "/home",
        views: {
            'home-tab': {
                templateUrl: "templates/home.html",
                controller: 'HomeController'
            }
        }
    })
    .state('app.about', {
        url: "/about",
        views: {
            'about-tab': {
                templateUrl: "templates/about.html"
            }
        }
    })
    .state('app.contact', {
        url: "/contact",
        views: {
            'contact-tab': {
                templateUrl: "templates/contact.html",
                controller: 'ContactController'
            }
        }
    })
    .state('app.details', {
        url: '/details/:id',
        views: {
            'details-tab': {
                templateUrl: 'templates/details.html',
                controller: 'DetailsController'
            }
        }
    })
    .state('app.order', {
        url: '/order',
        views: {
            'order-tab': {
                templateUrl: 'templates/order.html',
                controller: 'OrderController'
            }
        }
    });
    $urlRouterProvider.otherwise("/app/home");
});