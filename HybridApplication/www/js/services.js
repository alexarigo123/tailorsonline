"use strict";

angular.module("tailorsOnlineApp")
    .constant("baseURL", "https://tailorsonlinerestapi-silkier-monoclinism.mybluemix.net/")
    .config(function($httpProvider) {
        $httpProvider.interceptors.push("authInterceptor");
    })
    .factory("authInterceptor", ["auth", function(auth) {
        return {
            // automatically attach Authorization header
            request: function(config) {
                if(auth.getToken())
                {
                    config.headers["x-access-token"] = auth.getToken();
                }

                return config;
            },
            // If a token was sent back, save it
            response: function(res) {
                if(res.data.success && res.data.token)
                {
                    auth.saveToken(res.data.token);
                }

                return res;
            },
            responseError: function (res) {
                if(res.status === 401 && res.data.message.indexOf("Wrong password.") === -1) {
                    auth.deleteToken();
                    try
                    {
                        location.reload();
                    }
                    catch(err){}
                }

                return res;
            }
        };
    }])
    .service("auth", function($window) {
        var self = this;
        self.getCurrentUserName = function() {
            var token = self.getToken();
            if (token)
            {
                var base64Url = token.split(".")[1];
                var base64 = base64Url.replace("-", "+").replace("_", "/");

                return JSON.parse($window.atob(base64))._doc.name;
            }
            else
            {
                return "";
            }
        };
        self.getCurrentUserEmail = function() {
            var token = self.getToken();
            if (token)
            {
                var base64Url = token.split(".")[1];
                var base64 = base64Url.replace("-", "+").replace("_", "/");

                return JSON.parse($window.atob(base64))._doc.email;
            }
            else
            {
                return "";
            }
        };
        self.getCurrentUserCellphone = function() {
            var token = self.getToken();
            if (token)
            {
                var base64Url = token.split(".")[1];
                var base64 = base64Url.replace("-", "+").replace("_", "/");

                return JSON.parse($window.atob(base64))._doc.cellphone || "";
            }
            else
            {
                return "";
            }
        };
        self.userLoggedIn = function()
        {
            return typeof $window.localStorage.authJWT !== "undefined";
        };
        self.saveToken = function(token) {
            $window.localStorage.authJWT = token;
        };
        self.getToken = function() {
            return $window.localStorage.authJWT;
        };
        self.deleteToken = function() {
            $window.localStorage.removeItem("authJWT");
        };
    })
    .service("cache", function($window) {
        var self = this;
        self.addObject = function(key, object)
        {
            $window.localStorage[key] = JSON.stringify(object);
        };
        self.getObject = function(key)
        {
            var object = "null";
            if ($window.localStorage[key])
            {
                object = $window.localStorage[key];
            }

            return JSON.parse(object);
        };
        self.delete = function(key)
        {
            $window.localStorage.removeItem(key);
        };
    })
    .factory("categoryFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.categories = function() {
            return $resource(baseURL + "categories/", null, { query: { method: "GET" }});
        };

        return actions;
    }])
    .factory("contactFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.contact = function() {
            return $resource(baseURL + "contact/", null, { send: { method: "POST" }});
        };

        return actions;
    }])
    .factory("commentFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.comments = function() {
            return $resource(baseURL + "comments/", null, { query: { method: "GET" }, create: { method: "POST" }});
        };

        return actions;
    }])
    .factory("designFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.designs = function() {
            return $resource(baseURL + "designs/", null, null);
        };

        return actions;
    }])
    .factory("fabricFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.fabrics = function() {
            return $resource(baseURL + "fabrics/", null, { query: { method: "GET" }});
        };

        return actions;
    }])
    .factory("orderFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.orders = function() {
            return $resource(baseURL + "orders/", null, { create: { method: "POST"}});
        };

        return actions;
    }])
    .factory("userFactory", ["$resource", "baseURL", function($resource, baseURL) {
        var actions = {};

        actions.users = function() {
            return $resource(baseURL + "users/", null, { create: { method: "POST"}});
        };
        actions.userLogin = function() {
            return $resource(baseURL + "users/login", null, { authenticate: {method: "POST"}});
        };

        return actions;
    }]);